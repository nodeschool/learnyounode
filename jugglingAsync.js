const http = require("http");
const urls = process.argv.slice(2);

const getContent = url =>
  new Promise((resolve, reject) => {
    http.get(url, res => {
      let rawData = "";
      res.on("data", data => {
        rawData += data;
      });
      res.on("end", () => resolve(rawData));
      res.on("error", err => reject(err));
    });
  });

Promise.all(urls.map(getContent)).then(data => {
  data.forEach(res => {
    console.log(res);
  });
});
