const http = require('http')

const [,, url] = process.argv

http.get(url, res => {
    // number of code units, not unicode characters
    let strLen = 0
    let str = ''
    res.setEncoding('utf8')
    res.on('data', data => {
        strLen += data.length
        str += data
    })
    res.on('end', () => {
        console.log(strLen)
        console.log(str)
    })
})