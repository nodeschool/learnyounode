const fs = require('fs')

module.exports = (dirName, fileExtension, callback) => {
    fs.readdir(dirName, {}, (err, data) => {
        if (err) return callback(err)
        callback(null, data.filter(el => el.includes('.' + fileExtension)))
    })
}