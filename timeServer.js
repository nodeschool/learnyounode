const net = require('net')

const [,, port] = process.argv
const now = () => {
    const date = new Date()
    const year = String(date.getFullYear())
    const month = String(date.getMonth() + 1).padStart(2, 0)
    const dateOfMonth = String(date.getDate()).padStart(2, 0)
    const hour = String(date.getHours()).padStart(2, 0)
    const minutes = String(date.getMinutes()).padStart(2, 0)

    return `${year}-${month}-${dateOfMonth} ${hour}:${minutes}`
}

const server = net.createServer(socket => {
    socket.end(`${now()}\n`, 'utf8')
})

server.listen(port)
