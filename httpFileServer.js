const http = require('http')
const fs = require('fs')

const [,, port, file] = process.argv
const stream = fs.createReadStream(file)

const server = http.createServer((req, res) => {
    stream.pipe(res)
})

server.listen(port)