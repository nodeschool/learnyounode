const http = require('http')
const port = process.argv[2] || 8192

const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        req.on('end', () => {
            res.end(body.toUpperCase());
        });
    }
})

server.listen(port)