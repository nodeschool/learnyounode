const http = require('http')

const [,, port] = process.argv

const server = http.createServer((req, res) => {
    let jsonResponse = {}
    if (/\/api\/unixtime/.test(req.url)) {
        let time = req.url.match(/\/api\/unixtime\?iso=(.+)/)[1]
        let date = new Date(time)
        const jsonResponse = JSON.stringify({
            "unixtime": date.getTime()
        })
        res.end(jsonResponse)
    } else if (/\/api\/parsetime/.test(req.url)) {
        let time = req.url.match(/\/api\/parsetime\?iso=(.+)/)[1]
        let date = new Date(time)
        const jsonResponse = JSON.stringify({
            "hour": date.getHours(),
            "minute": date.getMinutes(),
            "second": date.getSeconds()
        })
        res.end(jsonResponse)
    }
})

server.listen(port)